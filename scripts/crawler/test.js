// http://potter-pod.com/Five%20Star%20Books%20(EPUB)/EPUB/
var crawler = require('splitzee-crawler').Crawler;
var urli = require('url');
var request = require('request');
var progress = require('progress');
var bar = new progress('Crawling [:bar] :total targets :percent :etas', { total: 1, width:25,complete:'X',incomplete:'.' });


var c = new crawler({
	maxConnections:1,
	cache:1,
	userAgent:"PubDirBot v1.0 (http://pubdir.com/bot)",
	callback:handleContent
});

function handleFile(url) {
	request(url, {method: 'HEAD'}, function (err, res, body){
		var obj = {
			url : url,
			date : res.headers.date,
			size : res.headers['content-length'],
			type : res.headers['content-type']
		}
		console.log(obj);
		process.exit();
	});
}

function handleContent(err,results,$) {
	if (err) {
		throw err;
	}
	if ($ !== undefined) {
		if (isPub($.html())) {
			$("a").each(function(index,a) {
				var href = a.attribs.href;
				var parts = urli.parse(href);
				if (isRelative(href) && (parts.search === null)) {
					var target = urli.resolve(results.request.uri.href,href);
					if (target.substr(-1) !== '/') { //File, not dir
						handleFile(target);
					} else { //Directory, spider it
						c.queue({
							uri:target,
							callback:handleContent
						});
						bar.total++;
					}
				}
			});
		} else {
			console.log('Not a public directory.');
		}
	}
	bar.tick();
}

function isPub(body) { //Test to see if HTML looks like a public directory
	if (!body.indexOf('Index of /')) {
		return false;
	}
	if (!body.indexOf('Last modified')) {
		return false;
	}
	if (!body.indexOf('Size')) {
		return false;
	}
	return true;
}

function isRelative(url) {
	var r = new RegExp('^(?:[a-z]+:)?//', 'i');
	return !r.test(url);
}

c.queue({
	uri:'http://potter-pod.com/Five%20Star%20Books%20(EPUB)/EPUB/',
	callback:handleContent
});
c.onDrain = function() {
	console.log('Que is empty.');
	process.exit();
}
