//Tests if URL is public directory, if so then gathers all listings in public dir
var bunyan = require('bunyan');
var request = require('request');
var cheerio = require('cheerio');
var urli = require('url');

var log = bunyan.createLogger({name:'pubScraper'});
var mapped = [];

function pubScrape(url,callback) {
	var baseUrl = url;
	var options = {
		url:url,
		headers : {
			'User-Agent':'PubDir.com Bot (http://www.pubdir.com/bot)'
		}
	}
	log.info('Scraping : '+url);
	request(options, function (error, response, body) {
		var r = new RegExp('^(?:[a-z]+:)?//', 'i');
		var addCount = 0;
		if (!error && response.statusCode == 200) {
			var $ = cheerio.load(body);
			var urlsToCrawl = [];
			$('a').each(function(index, a) {
				var target = $(a).attr('href');
				if (!r.test(target)) {
					addCount++;
					urlsToCrawl.push( urli.resolve( baseUrl, target ) );	
				}
			});
			log.warn('Added '+addCount+' urls');
			if (urlsToCrawl.length ==  0) {
				callback();
			}	
			pubScrape(urlsToCrawl.pop(),callback);
		} else {
			callback(null,'Not found, error '+response.statusCode);
		} 
	});
}


log.info('Enter pubScraper');
pubScrape('http://www.shadowsgovernment.com/shadows-library/',function(results,error) {
	log.info('Page downloaded');
	if (error) {
		log.error(error);
		process.exit();
	} else {
		results.each(function(index,url) {
			
		});
	}
});
